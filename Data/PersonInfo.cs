﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class PersonInfo
    {
        public int Id { get; set; }
        public string FullNameText { get; set; }
        public int Age { get; set; }
        public string BirthPlace { get; set; }
    }
}
