﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
   public class DataContext: DbContext
    {
        public DataContext(DbContextOptions<DataContext> options):base(options)
        { }

        public DbSet<PersonInfo> PersonInfo { get; set; }
    }
}
