﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Progress_assignment.Models;
using Services;
using Kendo.Mvc.Extensions;

namespace Progress_assignment.Controllers
{
    public class HomeController : Controller
    {
     

        private readonly IListNames service;

        public HomeController( IListNames service)
        {
            
            this.service = service;
            
        }

        public IActionResult Index()
        {

            return View();
        }

        public IActionResult GetData([DataSourceRequest]DataSourceRequest request)
        {
           
            var a = ListNames(request).ToList();
            List<FullCustomerModel> result = new List<FullCustomerModel>();

             return  Json(a.ToDataSourceResult(request));
        }
        private IEnumerable<FullCustomerModel> ListNames(DataSourceRequest request)
        {
            
            var data =  service.ReturnNames(1).Result.Select
            (x => new FullCustomerModel
            {
                FullName = x.FullNameText,
                Age=x.Age,
                BirthPlace=x.BirthPlace
            });
            return data.ToList();
        }

        //private IList<string> RequestToList( DataSourceRequest request)
        //{
        //    var requestedList = new List<string>();
        //    requestedList.Add(request.);

        //}
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
