﻿using Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public interface IListNames
    {
        
        Task<List<PersonInfo>> ReturnNames(int filter);
    }
}