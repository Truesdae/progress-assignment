﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Services
{
    public class ListNames : IListNames
    {
        public DataContext context { get; }

        public ListNames(DataContext context)
        {
            this.context = context;
        }


        public async Task<List<PersonInfo>> ReturnNames(int filter)
        {      
            return context.PersonInfo.ToList();

        } 
    }
}
